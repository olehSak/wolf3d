/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/25 13:37:16 by osak              #+#    #+#             */
/*   Updated: 2018/10/25 13:37:19 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		player_pos(t_window *win, int y, int x)
{
	int x1;
	int y1;

	y1 = -1;
	while (++y1 < y)
	{
		x1 = -1;
		while (++x1 < x)
			if (win->map[y1][x1] == 0)
			{
				win->set->posy = x1 + 0.5;
				win->set->posx = y1 + 0.5;
				return (1);
			}
	}
	return (0);
}
