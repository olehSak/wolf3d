/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_validate_func.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/20 16:36:58 by osak              #+#    #+#             */
/*   Updated: 2018/10/20 16:37:05 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		line_validator(int **map, int x, int y)
{
	int		x1;
	int		y1;

	x1 = -1;
	y1 = -1;
	while (++x1 < x)
	{
		if (map[0][x1] == 0)
			return (0);
		if (map[y - 1][x1] == 0)
			return (0);
	}
	while (++y1 < y)
	{
		if (map[y1][0] == 0)
			return (0);
		if (map[y1][x - 1] == 0)
			return (0);
	}
	return (1);
}

void	error_handler(int c_h, int c_v, int all_hor_pts)
{
	if (c_h == 0 || c_v == 0)
	{
		ft_putstr("WRONG MAP\n");
		exit(1);
	}
	if (all_hor_pts % c_v > 0 || all_hor_pts % c_h > 0)
	{
		ft_putstr("ERROR MAP\n");
		exit(1);
	}
}
