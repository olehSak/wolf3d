/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/19 19:28:22 by osak              #+#    #+#             */
/*   Updated: 2018/10/19 19:28:26 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		ft_isspace(char *c)
{
	int		it;

	it = 0;
	while (c[it] < 33)
		it++;
	it = 0;
	while (c[it] != 0)
	{
		if (ft_isalpha(c[it]))
		{
			ft_putstr("CHAR IN MAP");
			exit(1);
		}
		it++;
	}
	return (ft_atoi(c));
}

int		map_int(int fd, int **istr, int x, int y)
{
	char	**sstr;
	char	*str;

	while (get_next_line(fd, &str))
	{
		sstr = ft_strsplit(str, ' ');
		x = 0;
		while (sstr[x])
		{
			istr[y][x] = ft_isspace(sstr[x]);
			if (istr[y][x] < 0)
				return (0);
			free(sstr[x]);
			x++;
		}
		free(sstr);
		free(str);
		y++;
	}
	free(str);
	return (1);
}

int		create_map(t_window *win, int c_v, int c_h, char **argv)
{
	int			fd;

	win->map = (int **)malloc(sizeof(int *) * c_v);
	while (--c_v > -1)
		win->map[c_v] = (int *)malloc(sizeof(int) * c_h);
	if ((fd = open(argv[1], O_RDONLY)) < 0)
	{
		ft_putstr("Cannot open file.\n");
		exit(1);
	}
	if (!map_int(fd, win->map, 0, 0))
	{
		ft_putstr("ERROR\n");
		exit(1);
	}
	return (1);
}

int		read_file(char **argv, int *c_v, int *c_h)
{
	char		*str;
	char		**easter;
	int			all_hor_pts;
	int			fd;

	all_hor_pts = 0;
	if ((fd = open(argv[1], O_RDONLY)) < 0)
	{
		ft_putstr("Cannot open file.\n");
		exit(1);
	}
	while (get_next_line(fd, &str))
	{
		easter = ft_strsplit(str, ' ');
		*c_h = -1;
		while (easter[++(*c_h)] && ++all_hor_pts)
			free(easter[*c_h]);
		free(easter);
		free(str);
		(*c_v)++;
	}
	free(str);
	error_handler(*c_h, *c_v, all_hor_pts);
	close(fd);
	return (1);
}

int		validate_map(t_window *win, char **argv)
{
	int		*x;
	int		*y;

	x = ft_memalloc(1);
	y = ft_memalloc(1);
	if (!read_file(argv, y, x))
		return (0);
	if (!create_map(win, *y, *x, argv))
		return (0);
	if (!line_validator(win->map, *x, *y))
		return (0);
	if (!player_pos(win, *y, *x))
		return (0);
	free(x);
	free(y);
	return (1);
}
