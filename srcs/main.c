/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 13:36:15 by osak              #+#    #+#             */
/*   Updated: 2018/08/08 13:36:17 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	setup_std(t_window *window, int bpp, int endian)
{
	int		size_line;
	t_scene	*scene;
	t_line	*line;

	window->m_ptr = mlx_init();
	window->w_ptr = mlx_new_window(window->m_ptr, WINW, WINH, WINNAME);
	window->img = mlx_new_image(window->m_ptr, WINW, WINH);
	window->data =
			(int *)mlx_get_data_addr(window->img, &bpp, &size_line, &endian);
	line = (t_line*)malloc(sizeof(t_line));
	scene = (t_scene*)malloc(sizeof(t_scene));
	window->set = (t_settings*)malloc(sizeof(t_settings));
	window->set->dirx = 0;
	window->set->diry = -0.999;
	window->set->planex = -0.66;
	window->set->planey = 0;
	window->set->x_mouse = -1;
	window->line = line;
	window->scene = scene;
	ft_memset(window->data, 0, WINW * WINH * 4);
}

int		check_file_name(char *file_name)
{
	size_t	len;

	len = ft_strlen(file_name);
	if (file_name[len - 1] != 'd' &&
		file_name[len - 2] != '3' &&
		file_name[len - 3] != 'w' &&
		file_name[len - 4] != '.')
		return (1);
	return (0);
}

int		main(int argc, char **argv)
{
	t_window	*window;

	window = (t_window*)malloc(sizeof(t_window));
	window->tex_data = (t_texture*)malloc(sizeof(t_texture));
	window->tex_data->texture = (int**)malloc(sizeof(int*) * 6);
	setup_std(window, 0, 0);
	if (argc != 2 || check_file_name(argv[1]))
	{
		ft_putstr("Wrong file.\n");
		exit(1);
	}
	if (!validate_map(window, argv))
	{
		ft_putstr("ERROR MAP\n");
		exit(1);
	}
	raycaster(window);
	mlx_clear_window(window->m_ptr, window->w_ptr);
	mlx_put_image_to_window(window->m_ptr, window->w_ptr, window->img, 0, 0);
	mlx_hook(window->w_ptr, 6, 0, mouse_move, window);
	mlx_hook(window->w_ptr, 2, 3, system_c, window);
	mlx_hook(window->w_ptr, 2, 3, readkeys, window);
	mlx_hook(window->w_ptr, 17, 1L << 17, event, 0);
	mlx_loop(window->m_ptr);
	return (0);
}
