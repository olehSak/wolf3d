/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   system.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 13:35:36 by osak              #+#    #+#             */
/*   Updated: 2018/08/08 13:35:41 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	walkalone(t_window *win)
{
	double	movespeed;
	int		m;

	movespeed = 0.5;
	m = win->set->move;
	if (win->map[(int)(win->set->posx + win->set->dirx * movespeed * m)]
		[(int)win->set->posy] == 0)
		win->set->posx += win->set->dirx * movespeed * m;
	if (win->map[(int)win->set->posx]
		[(int)(win->set->posy + win->set->diry * movespeed * m)] == 0)
		win->set->posy += win->set->diry * movespeed * m;
	win->set->move = 0;
}

int		readkeys(int key, t_window *win)
{
	win->set->rotate = 0;
	win->set->move = 0;
	if (key == 126 || key == 125)
	{
		key == 126 ? (win->set->move = 1)
				: (win->set->move = -1);
		walkalone(win);
	}
	if (key == 124 || key == 123)
	{
		key == 124 ? (win->set->rotate = -1)
				: (win->set->rotate = 1);
		rotation(win, 0, 0);
	}
	redraw(win);
	key == 53 ? exit(1) : 0;
	return (0);
}

int		system_c(int key, t_window *window)
{
	key == 53 ? exit(1) : 0;
	ft_putstr("Key =\t");
	ft_putnbr(key);
	ft_putstr("\n");
	ft_putstr("window->mas =\t");
	ft_putnbr((int)window->mas);
	ft_putstr("\n");
	return (0);
}

int		event(int key)
{
	if (key == 53 || !key)
		exit(1);
	return (0);
}
