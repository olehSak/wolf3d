/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 13:35:17 by osak              #+#    #+#             */
/*   Updated: 2018/08/08 13:35:20 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	redraw(t_window *win)
{
	mlx_clear_window(win->m_ptr, win->w_ptr);
	raycaster(win);
	mlx_put_image_to_window(win->m_ptr, win->w_ptr, win->img, 0, 0);
}

void	rotation(t_window *win, double dirx, double diry)
{
	double	rot_s;
	int		r;
	double	olddirx;
	double	oldplanex;

	rot_s = 0.2;
	r = win->set->rotate;
	dirx = win->set->dirx;
	diry = win->set->diry;
	olddirx = dirx;
	win->set->dirx = dirx * cos(rot_s * r) - diry * sin(rot_s * r);
	win->set->diry = olddirx * sin(rot_s * r) + diry * cos(rot_s * r);
	oldplanex = win->set->planex;
	win->set->planex = win->set->planex * cos(rot_s * r)
					- win->set->planey * sin(rot_s * r);
	win->set->planey = oldplanex * sin(rot_s * r)
					+ win->set->planey * cos(rot_s * r);
	win->set->rotate = 0;
}

void	m_rotation(t_window *win, double rot_s, double r)
{
	double	dirx;
	double	diry;
	double	olddirx;
	double	oldplanex;

	dirx = win->set->dirx;
	diry = win->set->diry;
	r = win->set->rotate;
	olddirx = dirx;
	win->set->dirx = dirx * cos(rot_s * r) - diry * sin(rot_s * r);
	win->set->diry = olddirx * sin(rot_s * r) + diry * cos(rot_s * r);
	oldplanex = win->set->planex;
	win->set->planex = win->set->planex * cos(rot_s * r)
					- win->set->planey * sin(rot_s * r);
	win->set->planey = oldplanex * sin(rot_s * r)
					+ win->set->planey * cos(rot_s * r);
	win->set->rotate = 0;
}

int		mouse_move(int x, int y, t_window *win)
{
	if ((x >= 0 && y >= 0) && (x < WINW && y < WINH))
	{
		if (x > win->set->x_mouse)
			win->set->rotate = -(1 - x / WINW);
		else
			win->set->rotate = (1 - x / WINW);
		win->set->x_mouse = x;
		m_rotation(win, 0.05, 0);
		redraw(win);
		return (1);
	}
	return (0);
}
