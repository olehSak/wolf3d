/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   floor.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/23 17:49:50 by osak              #+#    #+#             */
/*   Updated: 2018/10/23 17:49:51 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	floor_side(t_scene *scene, t_texture *tex)
{
	if (scene->side == 0 && scene->raydirx > 0)
	{
		tex->floorxw = scene->mapx;
		tex->flooryw = scene->mapy + tex->wallx;
	}
	else if (scene->side == 0 && scene->raydirx < 0)
	{
		tex->floorxw = scene->mapx + 1.0;
		tex->flooryw = scene->mapy + tex->wallx;
	}
	else if (scene->side == 1 && scene->raydiry > 0)
	{
		tex->floorxw = scene->mapx + tex->wallx;
		tex->flooryw = scene->mapy;
	}
	else
	{
		tex->floorxw = scene->mapx + tex->wallx;
		tex->flooryw = scene->mapy + 1.0;
	}
	if (tex->drawend < 0)
		tex->drawend = WINH;
}

void	floor_calc(t_scene *s, t_texture *tex, t_settings *set, t_line *line)
{
	double	distwall;
	double	distplayer;
	double	weight;
	double	currentdist;

	distwall = s->perpwalldist;
	distplayer = 0.0;
	floor_side(s, tex);
	tex->y = tex->drawend + 1;
	while (tex->y++ < WINH)
	{
		currentdist = WINH / (2.0 * tex->y - WINH);
		weight = (currentdist - distplayer) / (distwall - distplayer);
		tex->texx = (int)((weight * tex->floorxw + (1.0 - weight) *
				set->posx) * 64) % 64;
		tex->texy = (int)((weight * tex->flooryw + (1.0 - weight) *
				set->posy) * 64) % 64;
		line->color = (tex->texture[3][64 * tex->texy + tex->texx] >> 1)
				& 8355711;
		line->data[tex->y * WINW + tex->x] = line->color;
	}
}
