/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scene.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/16 15:52:45 by osak              #+#    #+#             */
/*   Updated: 2018/10/16 15:52:49 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	calculate(t_scene *s, t_window *win, int x)
{
	s->camerax = 2 * x / (double)(WINW) - 1;
	s->raydirx = win->set->dirx + win->set->planex * s->camerax;
	s->raydiry = win->set->diry + win->set->planey * s->camerax;
	s->mapx = (int)win->set->posx;
	s->mapy = (int)win->set->posy;
	s->deltadistx = fabs(1.0 / s->raydirx);
	s->deltadisty = fabs(1.0 / s->raydiry);
	s->hit = 0;
	s->side = 0;
	s->raydirx < 0 ? (s->stepx = -1) :
		(s->stepx = 1);
	s->raydirx < 0 ? (s->sidedistx = (win->set->posx - s->mapx)
			* s->deltadistx)
					: (s->sidedistx = (s->mapx + 1.0 - win->set->posx)
							* s->deltadistx);
	s->raydiry < 0 ? (s->stepy = -1) :
		(s->stepy = 1);
	s->raydiry < 0 ? (s->sidedisty = (win->set->posy - s->mapy) * s->deltadisty)
					: (s->sidedisty = (s->mapy + 1.0 - win->set->posy)
						* s->deltadisty);
}

void	perform_dda(t_scene *s, t_window *win)
{
	while (s->hit == 0)
	{
		if (s->sidedistx < s->sidedisty)
		{
			s->sidedistx += s->deltadistx;
			s->mapx += s->stepx;
			s->side = 0;
		}
		else
		{
			s->sidedisty += s->deltadisty;
			s->mapy += s->stepy;
			s->side = 1;
		}
		if (win->map[s->mapx][s->mapy] > 0)
			s->hit = 1;
	}
	if (s->side == 0)
		s->perpwalldist = (s->mapx - win->set->posx + (1 - (float)s->stepx)
													/ 2) / s->raydirx;
	else
		s->perpwalldist = (s->mapy - win->set->posy + (1 - (float)s->stepy)
													/ 2) / s->raydiry;
}

void	calculate_draw(t_line *line, t_texture *tex, t_scene *s, t_window *win)
{
	line->lineheight = (int)(WINH / s->perpwalldist);
	tex->drawstart = (WINH / 2) - (line->lineheight / 2);
	tex->drawstart < 0 ? (tex->drawstart = 0) : 0;
	tex->drawend = (WINH / 2) + line->lineheight / 2;
	tex->drawend >= WINH ? (tex->drawend = WINH - 1) : 0;
	line->data = win->data;
	tex->texnum = win->map[s->mapx][s->mapy] - 1;
	if (s->side == 0)
		tex->wallx = win->set->posy + s->perpwalldist * s->raydiry;
	else
		tex->wallx = win->set->posx + s->perpwalldist * s->raydirx;
	tex->wallx -= floor(tex->wallx);
}

void	texture_calc(t_scene *scene, t_texture *tex, t_line *line)
{
	tex->texx = (int)(tex->wallx * (double)(tex->width));
	if (scene->side == 0 && scene->raydirx > 0)
		tex->texx = tex->width - tex->texx - 1;
	if (scene->side == 1 && scene->raydiry < 0)
		tex->texy = tex->height - tex->texy - 1;
	tex->y = tex->drawstart - 1;
	while (++tex->y < tex->drawend)
	{
		tex->d = tex->y * 256 - WINH * 128 + line->lineheight * 128;
		tex->texy = ((tex->d * tex->height) / line->lineheight) / 256;
		tex->texnum = 0;
		if (scene->side == 0 && scene->raydirx > 0)
			tex->texnum = 0;
		else if (scene->side == 0 && scene->raydirx < 0)
			tex->texnum = 1;
		else if (scene->side == 1 && scene->raydiry > 0)
			tex->texnum = 2;
		else
			tex->texnum = 3;
		line->color = tex->texture[tex->texnum][tex->height *
										tex->texy + tex->texx];
		line->data[tex->x + tex->y * WINW] = line->color;
	}
}
