/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   texture.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/28 13:34:18 by osak              #+#    #+#             */
/*   Updated: 2018/09/28 13:34:22 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	get_xpm_data(t_window *win, t_texture *tex, int bpp, int endian)
{
	int		size_line;
	void	*data;
	char	*str;

	str = 0;
	tex->texnum == 0 ? str = "XPMtextures/Bricks.xpm" : 0;
	tex->texnum == 1 ? str = "XPMtextures/bookshelf.xpm" : 0;
	tex->texnum == 2 ? str = "XPMtextures/Bark.xpm" : 0;
	tex->texnum == 3 ? str = "XPMtextures/GlassFacete.xpm" : 0;
	tex->texnum == 4 ? str = "XPMtextures/Mud.xpm" :
			"XPMtextures/WalkStone.xpm";
	tex->texnum == 5 ? str = "XPMtextures/WalkStone.xpm" : 0;
	data = mlx_xpm_file_to_image(win->m_ptr, str, &tex->width, &tex->height);
	tex->texture[tex->texnum] =
			(int*)mlx_get_data_addr(data, &bpp, &size_line, &endian);
}

void	generate_texture(t_texture *tex, t_window *win)
{
	tex->width = 64;
	tex->height = 64;
	tex->texnum = -1;
	while (++tex->texnum < 6)
		get_xpm_data(win, tex, 0, 0);
}
