/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raycaster.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/10 15:24:38 by osak              #+#    #+#             */
/*   Updated: 2018/09/10 15:24:41 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	sky_ground_box(t_window *win)
{
	int		y;
	int		x;

	y = WINH;
	while (y > 0)
	{
		y--;
		x = WINW;
		while (x-- > 0)
		{
			if (y < WINH / 2)
				win->data[y * WINW + x] = SKYBOX;
			else
				win->data[y * WINW + x] = GROUND;
		}
	}
}

void	raycaster(t_window *win)
{
	sky_ground_box(win);
	generate_texture(win->tex_data, win);
	win->tex_data->x = -1;
	while (++win->tex_data->x < WINW)
	{
		calculate(win->scene, win, win->tex_data->x);
		perform_dda(win->scene, win);
		calculate_draw(win->line, win->tex_data, win->scene, win);
		texture_calc(win->scene, win->tex_data, win->line);
		floor_calc(win->scene, win->tex_data, win->set, win->line);
	}
}
