/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   header.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <osak@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/22 15:20:53 by osak              #+#    #+#             */
/*   Updated: 2018/10/22 15:20:58 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_HEADER_H
# define WOLF3D_HEADER_H

# define WINNAME "WOLF3D"

# define WINW 1200
# define WINH 700
# define SKYBOX 0x99ffff
# define GROUND 0x331900

# include <time.h>
# include "mlx.h"
# include "math.h"
# include "get_next_line.h"

typedef struct	s_line
{
	int			color;
	int			lineheight;
	int			*data;
}				t_line;

typedef struct	s_scene
{
	double		camerax;
	double		raydirx;
	double		raydiry;
	int			mapx;
	int			mapy;
	double		deltadistx;
	double		deltadisty;
	double		sidedistx;
	double		sidedisty;
	double		perpwalldist;
	int			stepx;
	int			stepy;
	int			hit;
	int			side;
}				t_scene;

typedef struct	s_settings
{
	double		posx;
	double		posy;
	double		dirx;
	double		diry;
	double		planex;
	double		planey;
	int			rotate;
	int			move;
	int			x_mouse;
}				t_settings;

typedef struct	s_texture
{
	int			width;
	int			height;
	int			**texture;
	int			texnum;
	double		wallx;
	int			texx;
	int			texy;
	int			y;
	int			x;
	int			d;
	int			drawstart;
	int			drawend;
	double		floorxw;
	double		flooryw;
}				t_texture;

typedef struct	s_window
{
	t_scene		*scene;
	t_settings	*set;
	void		*m_ptr;
	void		*w_ptr;
	void		*img;
	int			*data;
	float		mas;
	int			**map;
	t_line		*line;
	t_texture	*tex_data;
}				t_window;

int				mouse_move(int x, int y, t_window *window);
void			walkalone(t_window *win);
void			rotation(t_window *win, double dirx, double diry);
int				readkeys(int key, t_window *win);
int				event(int key);
int				system_c(int key, t_window *window);
void			redraw(t_window *window);
void			raycaster(t_window *win);
void			generate_texture(t_texture *tex, t_window *window);
void			perform_dda(t_scene *scene, t_window *window);
void			calculate(t_scene *scene, t_window *win, int x);
void			texture_calc(t_scene *scene, t_texture *tex, t_line *line);
void			calculate_draw(t_line *line, t_texture *tex, t_scene *scene,
		t_window *win);
void			floor_calc(t_scene *scene, t_texture *tex, t_settings *set,
		t_line *line);
int				validate_map(t_window *win, char **argv);
int				line_validator(int **map, int x, int y);
void			error_handler(int c_h, int c_v, int all_hor_pts);
int				player_pos(t_window *window, int x, int y);

#endif
