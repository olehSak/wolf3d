#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: osak <osak@student.unit.ua>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/10/23 19:35:10 by osak              #+#    #+#              #
#    Updated: 2018/10/23 19:35:13 by osak             ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = wolf3d

CC = gcc

CFLAGS = -Wall -Wextra -Werror

GL = -framework OpenGL -framework AppKit 

HEADERS = headers/

SRCS = srcs/main.c\
	   srcs/get_next_line.c\
	   srcs/map/map.c\
	   srcs/map/map_validate_func.c\
	   srcs/map/player.c\
	   srcs/system/system.c\
	   srcs/screen/floor.c\
	   srcs/screen/raycaster.c\
	   srcs/screen/scene.c\
	   srcs/screen/texture.c\
	   srcs/screen/window.c

OBJ = $(SRCS:.c=.o)

LIBFTDIR = libft/

all: $(NAME)

$(NAME)	: $(OBJ) $(LIBFTDIR)libft.a
	$(CC) $(CFLAGS) $(GL) $(OBJ) -o $(NAME) -L./$(LIBFTDIR) -lft -L /usr/X11/lib -l mlx

%.o:%.c $(HEADERS)
	$(CC) $(CFLAGS) -c -o $@ $< -I $(HEADERS) -I $(LIBFTDIR) -I /usr/X11/include

$(LIBFTDIR)libft.a: libft/Makefile
	make -C libft/

clean:
	rm -f $(OBJ)
	make clean -C $(LIBFTDIR) 

fclean: clean
	rm -f $(NAME)
	make fclean -C $(LIBFTDIR) 

re: fclean all
	rm -f $(OBJ)
	make clean -C $(LIBFTDIR)

norm:
	norminette $(HEADERS)
	norminette $(SRCS)
	make norm -C $(LIBFTDIR)

rmsh:
	find . -name ".DS*" -o -name "._.*" -o -name "#*" -o -name "*~" -o -name "*.out" > rmsh
	xargs rm < rmsh
	rm rmsh