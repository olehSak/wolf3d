# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/Users/osak/projects/wolf3d/srcs/get_next_line.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/get_next_line.c.o"
  "/Users/osak/projects/wolf3d/srcs/main.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/main.c.o"
  "/Users/osak/projects/wolf3d/srcs/map/map.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/map/map.c.o"
  "/Users/osak/projects/wolf3d/srcs/map/map_validate_func.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/map/map_validate_func.c.o"
  "/Users/osak/projects/wolf3d/srcs/map/player.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/map/player.c.o"
  "/Users/osak/projects/wolf3d/srcs/screen/floor.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/screen/floor.c.o"
  "/Users/osak/projects/wolf3d/srcs/screen/raycaster.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/screen/raycaster.c.o"
  "/Users/osak/projects/wolf3d/srcs/screen/scene.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/screen/scene.c.o"
  "/Users/osak/projects/wolf3d/srcs/screen/texture.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/screen/texture.c.o"
  "/Users/osak/projects/wolf3d/srcs/screen/window.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/screen/window.c.o"
  "/Users/osak/projects/wolf3d/srcs/system/system.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/srcs/system/system.c.o"
  "/Users/osak/projects/wolf3d/trash/color.c" "/Users/osak/projects/wolf3d/cmake-build-debug/CMakeFiles/wolf3d.dir/trash/color.c.o"
  )
set(CMAKE_C_COMPILER_ID "AppleClang")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../libft"
  "../minilibx_macos"
  "../srcs"
  "../headers"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
