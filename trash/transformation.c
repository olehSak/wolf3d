/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transformation.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 13:35:48 by osak              #+#    #+#             */
/*   Updated: 2018/08/08 13:35:51 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

int		matrix(float *matrixofrotation, t_point xyzw)
{
	matrixofrotation[0] = 1.0f - 2.0f * (xyzw.y * xyzw.y + xyzw.z * xyzw.z);
	matrixofrotation[1] = 2.0f * (xyzw.x * xyzw.y + xyzw.z * xyzw.w);
	matrixofrotation[2] = 2.0f * (xyzw.x * xyzw.z - xyzw.y * xyzw.w);
	matrixofrotation[3] = 0.0f;
	matrixofrotation[4] = 2.0f * (xyzw.x * xyzw.y - xyzw.z * xyzw.w);
	matrixofrotation[5] = 1.0f - 2.0f * (xyzw.x * xyzw.x + xyzw.z * xyzw.z);
	matrixofrotation[6] = 2.0f * (xyzw.z * xyzw.y + xyzw.x * xyzw.w);
	matrixofrotation[7] = 0.0f;
	matrixofrotation[8] = 2.0f * (xyzw.x * xyzw.z + xyzw.y * xyzw.w);
	matrixofrotation[9] = 2.0f * (xyzw.y * xyzw.z - xyzw.x * xyzw.w);
	matrixofrotation[10] = 1.0f - 2.0f * (xyzw.x * xyzw.x + xyzw.y * xyzw.y);
	matrixofrotation[11] = 0.0f;
	matrixofrotation[12] = 0;
	matrixofrotation[13] = 0;
	matrixofrotation[14] = 0;
	matrixofrotation[15] = 1.0f;
	return (1);
}

t_point	ft_matvec(t_point p, float *m)
{
	t_point	npoint;

	npoint.x = m[0] * p.x + m[4] * p.y + m[8] * p.z + m[12] * p.w;
	npoint.y = m[1] * p.x + m[5] * p.y + m[9] * p.z + m[13] * p.w;
	npoint.z = m[2] * p.x + m[6] * p.y + m[10] * p.z + m[14] * p.w;
	npoint.w = m[3] * p.x + m[7] * p.y + m[11] * p.z + m[15] * p.w;
	return (npoint);
}

t_point	changer(t_point points, t_window *window)
{
	points.x = floorf((points.x * window->mas + window->disp_x) * (WINW));
	points.y = WINH - floorf((points.y * window->mas +
							window->disp_y) * (WINH));
	points.z = points.z;
	return (points);
}
