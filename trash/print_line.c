/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_line.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: osak <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/10 18:13:52 by osak              #+#    #+#             */
/*   Updated: 2018/09/10 18:13:55 by osak             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "header.h"

void	first(t_line *line, t_line resline)
{
	int		d;
	int		x;
	int		y;
	int		i;

	x = line->x0 + resline.x1;
	y = line->y0;
	i = 0;
	d = (resline.y0 << 1) - resline.x0;
	if (line->x0 < WINW && line->x0 >= 0 && line->y0 >= 0 && line->y0 < WINH)
		line->data[line->x0 + line->y0 * WINW] = line->color;
	while (++i <= resline.x0)
	{
		if (d > 0)
		{
			d += ((resline.y0 - resline.x0) << 1);
			y += resline.y1;
		}
		else
			d += (resline.y0 << 1);
		if (x < WINW && x >= 0 && y >= 0 && y < WINH)
			line->data[y * WINW + x] = line->color;
		x += resline.x1;
	}
}

void	second(t_line *line, t_line resline)
{
	int		d;
	int		y;
	int		x;
	int		i;

	i = 0;
	y = line->y0 + resline.y1;
	x = line->x0;
	d = (resline.x0 << 1) - resline.y0;
	if (line->x0 < WINW && line->x0 >= 0 && line->y0 >= 0 && line->y0 < WINH)
		line->data[line->x0 + line->y0 * WINW] = line->color;
	while (++i <= resline.y0)
	{
		if (d > 0)
		{
			d += ((resline.x0 - resline.y0) << 1);
			x += resline.x1;
		}
		else
			d += (resline.x0 << 1);
		if (x < WINW && x >= 0 && y >= 0 && y < WINH)
			line->data[y * WINW + x] = line->color;
		y += resline.y1;
	}
}

void	segment(t_line *line)
{
	t_line	resline;

	resline.x0 = ABS(line->x1 - line->x0);
	resline.y0 = ABS(line->y1 - line->y0);
	resline.x1 = line->x1 >= line->x0 ? 1 : -1;
	resline.y1 = line->y1 >= line->y0 ? 1 : -1;
	resline.y0 <= resline.x0 ? (first(line, resline)) : (second(line, resline));
}
